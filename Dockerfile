FROM alpine

LABEL maintainer="robert@scriptmyjob.com"

RUN apk update

RUN apk add python3

RUN apk add --no-cache --virtual \
            .pynacl_deps \
            build-base \
            python3-dev \
            libffi-dev \
            openssl-dev

RUN pip3 install --upgrade pip

RUN pip3 install ansible

RUN ansible --version | \
            grep -Eow '([0-9]+\.){2}[0-9]+' | \
            head -n 1 > /version.out

