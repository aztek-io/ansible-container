# README

## INFO

This is an automated build that will kick off at midnight (CDT) until the heat death of the universe or I stop paying my GitLab bill (whichever comes first).

```¯\_(ツ)_/¯```

This was created since Ansible does not maintain an updated Ansible image.

## HUB REPO

Here is the hub.docker.com repo:

https://hub.docker.com/r/aztek/ansible/
